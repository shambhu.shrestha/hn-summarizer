# HN Summarizer
Summarizes the top hackernews article and comments with the voice of your choice.

## Getting started

### Pre-requisites

1. OpenAI API Key
2. Eleven Labs API Key
3. Eleven Labs Voice ID of your favorite voice


### Setup
1. Create a python venv - I developed using python 3.11.3
2. Install the packages in `requirements.txt`
3. Prepare a file named `.env` in the same directory with the following contents:
```
OPENAI_API_KEY=<your openai api key>
ELEVEN_API_KEY=<your elevenlabs api key>
```
4. Modify `main.py` and update `VOICE_ID`

### Run
1. Execute `main.py`
2. To save the voice - you can call `elevenlabs.save`