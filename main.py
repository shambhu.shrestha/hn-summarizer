import json
import time

import openai
import requests
from dotenv import load_dotenv
from elevenlabs import Voice, VoiceSettings, generate, play
from openai import NotFoundError, OpenAI
from pydantic import BaseModel

# Load the .env file
# Should contain: OPENAI_API_KEY, ELEVEN_API_KEY
load_dotenv()


# Get this from "VoiceLab" tab of your Eleven account
VOICE_ID = "......"


def generate_audio(text):
    audio = generate(
        text=text,
        voice=Voice(
            voice_id=VOICE_ID,
            settings=VoiceSettings(
                stability=0.5, similarity_boost=0.8, style=0.0, use_speaker_boost=True
            ),
        ),
    )
    play(audio)


def display(messages, fn=print):
    for m in messages:
        fn(f"{m.role}: {m.content[0].text.value}")


class ToolBase(BaseModel):
    @classmethod
    def name(cls):
        raise NotImplementedError()

    @classmethod
    def description(cls):
        raise NotImplementedError()

    @classmethod
    def to_openai_function(cls):
        return {
            "name": cls.name(),
            "desciption": cls.description(),
            "parameters": cls.schema(),
        }


class GetHNItem(ToolBase):
    item_id: int

    @classmethod
    def name(cls):
        return "get_hn_item"

    @classmethod
    def description(cls):
        return "Fetches a specific item (Story or Comment) from Hacker News by its ID"

    def __call__(self):
        response = requests.get(
            f"https://hacker-news.firebaseio.com/v0/item/{self.item_id}.json"
        )
        if response.status_code == 200:
            return response.json()
        else:
            return None


TOOLS = [GetHNItem]
TOOLS_MAP = {tool.name(): tool for tool in TOOLS}


def assistant_tool_call(tool_call):
    # actually executes the tool call the OpenAI assistant wants to perform
    function = tool_call.function
    name = function.name
    arguments = json.loads(function.arguments)
    return {
        "tool_call_id": tool_call.id,
        "output": json.dumps(TOOLS_MAP[name](**arguments)()),
    }


client = OpenAI()


ASSISTANT_NAME = "Hacker News Summarizer"


def get_or_create_assistant():
    for assistant in openai.beta.assistants.list().data:
        if assistant.name == ASSISTANT_NAME:
            print(f"Found assistant with id: {assistant.id}")
            return assistant

    return client.beta.assistants.create(
        name="Hacker News Summarizer",
        instructions="""You specialize in providing dual summaries of Hacker News stories. Given a story ID, it retrieves the story's URL and comments, then crafts two distinct sections:

        1. Article Summary: This section condenses the main article's key points, focusing on technological aspects, major findings, or significant discussions. The summary is clear, concise, and informative.

        2. User Reactions: Here, the GPT summarizes the community's reactions in the comments, highlighting the most upvoted or significant comments to reflect the community's viewpoints and discussions.

        The GPT communicates in a professional yet approachable tone, balancing between brevity and detail. It maintains a neutral stance, avoiding personal opinions or speculations. When interacting with users, it uses a friendly and respectful approach, employing a generic greeting like "Hello" or "Hi there". It does not use personal names, ensuring a consistent and comfortable experience for all users. The GPT asks for clarifications when necessary and tailors its responses to offer insightful overviews of both the article and community feedback.""",
        model="gpt-3.5-turbo",
        tools=[
            {"type": "function", "function": tool.to_openai_function()}
            for tool in TOOLS
        ],
    )


def poll(run_id, thread_id):
    completed = False
    while not completed:
        try:
            print("polling the latest status")
            run = openai.beta.threads.runs.retrieve(run_id=run_id, thread_id=thread_id)
        # Above will raise NotFoundError when run creation is still in progress
        except NotFoundError:
            continue
        if run.status == "requires_action":
            print("requires_action")
            tool_outputs = []
            for tool_call in run.required_action.submit_tool_outputs.tool_calls:
                tool_output = assistant_tool_call(tool_call)
                tool_outputs.append(tool_output)
            openai.beta.threads.runs.submit_tool_outputs(
                thread_id=thread_id,
                run_id=run_id,
                tool_outputs=tool_outputs,
            )
        if run.status == "completed":
            completed = True
        time.sleep(0.5)
    return run


def start_thread(assistant, user_message):
    thread = client.beta.threads.create()
    print("Started thread. ID= ", thread.id)

    message = client.beta.threads.messages.create(
        thread_id=thread.id, role="user", content=user_message
    )
    print("Added User message to the thread. Creating a new run for assistant: ")

    run = client.beta.threads.runs.create(
        thread_id=thread.id,
        assistant_id=assistant.id,
    )

    run = poll(run.id, thread.id)
    messages = client.beta.threads.messages.list(thread_id=thread.id)
    display(messages, fn=generate_audio)


def get_top_hacker_news_item_id():
    # URL for Hacker News top stories
    top_stories_url = "https://hacker-news.firebaseio.com/v0/topstories.json"

    # Fetch the list of top stories
    response = requests.get(top_stories_url)
    if response.status_code != 200:
        return "Error fetching top stories."

    # Parse the JSON response which is a list of item IDs
    top_stories_ids = response.json()

    # Return the first item ID which is the top post
    return top_stories_ids[0] if top_stories_ids else "No top stories found."


if __name__ == '__main__':
    # Example usage
    top_id = get_top_hacker_news_item_id()
    print(f"Top hackernews post ID: {top_id}")
    assistant = get_or_create_assistant()
    start_thread(assistant, top_id)
